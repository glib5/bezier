import numpy as np
import matplotlib.pyplot as plt
from os import system


import bezier as Bez


 
#----------------------------------------------------------------------

def main():
    rand = np.random.RandomState(seed=4628)
    # create and organize control points (deg 1 to 4)
    i = 2
    n = 2
    control_points = rand.randint(n, size=(n, 2))
    idx = np.argsort(control_points[:, 0])
    control_points = control_points[idx]
    bezier_points = Bez.Bezier(control_points)
    plt.figure()
    plt.subplot(2, 2, i-1)
    plt.scatter(control_points[:, 0], control_points[:, 1], s=9, label='control points')
    plt.plot(control_points[:, 0], control_points[:, 1], linewidth=.4)
    plt.plot(bezier_points[:, 0], bezier_points[:, 1], color="r", linewidth=.6, label='bezier curve')
    plt.title("Bezier curve of degree %d"%(len(control_points)-1))
    plt.legend()
    i += 1
    n += 1
    control_points = rand.randint(10, size=(n, 2))
    idx = np.argsort(control_points[:, 0])
    control_points = control_points[idx]
    bezier_points = Bez.Bezier(control_points)   
    plt.subplot(2, 2, i-1)
    plt.scatter(control_points[:, 0], control_points[:, 1], s=9, label='control points')
    plt.plot(control_points[:, 0], control_points[:, 1], linewidth=.4)
    plt.plot(bezier_points[:, 0], bezier_points[:, 1], color="r", linewidth=.6, label='bezier curve')
    plt.title("Bezier curve of degree %d"%(len(control_points)-1))
    i += 1
    n += 1
    control_points = rand.randint(10, size=(n, 2))
    idx = np.argsort(control_points[:, 0])
    control_points = control_points[idx]
    bezier_points = Bez.Bezier(control_points)
    plt.subplot(2, 2, i-1)
    plt.scatter(control_points[:, 0], control_points[:, 1], s=9, label='control points')
    plt.plot(control_points[:, 0], control_points[:, 1], linewidth=.4)
    plt.plot(bezier_points[:, 0], bezier_points[:, 1], color="r", linewidth=.6, label='bezier curve')
    plt.title("Bezier curve of degree %d"%(len(control_points)-1))
    i += 1
    n += 1
    control_points = rand.randint(10, size=(n, 2))
    idx = np.argsort(control_points[:, 0])
    control_points = control_points[idx]
    bezier_points = Bez.Bezier(control_points)
    plt.subplot(2, 2, i-1)
    plt.scatter(control_points[:, 0], control_points[:, 1], s=9, label='control points')
    plt.plot(control_points[:, 0], control_points[:, 1], linewidth=.4)
    plt.plot(bezier_points[:, 0], bezier_points[:, 1], color="r", linewidth=.6, label='bezier curve')
    plt.title("Bezier curve of degree %d"%(len(control_points)-1))
    # random normal control points
    control_points = rand.normal(10, size=(9, 2))
    bezier_points = Bez.Bezier(control_points)
    Bez.bezier_plot(control_points, bezier_points)
    #
    control_points = np.asarray([[1, 0],
                                 [0, 1],
                                 [2, 2],
                                 [4, 1],
                                 [3, 0]])
    bezier_points = Bez.Bezier(control_points, 100)
    Bez.bezier_plot(control_points, bezier_points)
    #
    control_points = np.asarray([[2, 1],
                                 [4, 4],
                                 [1, 5],
                                 [3, 0]])
    bezier_points = Bez.Bezier(control_points, 100)
    Bez.bezier_plot(control_points, bezier_points)

    #---    
    plt.show()


if __name__=="__main__":
    main()






    
