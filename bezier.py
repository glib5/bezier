from os import system
from time import perf_counter as pc

import matplotlib.pyplot as plt
from numba import njit
import numpy as np
from scipy.special import binom

#------------------------------------------------------------------------------

def Bezier2(control_points, curve_points=30):
    '''given the control points, returns a bezier curve with N 'curve_points'
    as x-y values. control_points is a matrix of shape Nx2'''
    t = np.linspace(0, 1, curve_points)
    n, _ = control_points.shape # npoints
    bern = np.empty(shape=(n, curve_points))
    n -= 1 # curve degree
    for i in range(n+1):
        bern[i] = binom(n, i)*np.power(1-t, n-i)*np.power(t, i)
    return np.dot(bern.T, control_points.astype(np.float64))

#------------------------------------------------------------------------------

def __binom(n, k):
    return binom(n, k)

@njit
def __compute_bez(n, binc, control_points, curve_points):
    t = np.linspace(0, 1, curve_points)
    bern = np.empty(shape=(n, curve_points))
    n -= 1 # curve degree
    for i in range(n+1):
        bern[i] = binc[i]*np.power(1-t, n-i)*np.power(t, i)
    return np.dot(bern.T, control_points.astype(np.float64))

def Bezier(control_points, curve_points=30):
    n, _ = control_points.shape # npoints
    binc = __binom(n-1, np.arange(n))
    return __compute_bez(n, binc, control_points, curve_points)
    

#------------------------------------------------------------------------------

def bezier_plot(control_points, bezier_points):
    plt.figure()
    plt.scatter(control_points[:, 0],
                control_points[:, 1], s=9,
                label='control points')
    plt.plot(control_points[:, 0],
             control_points[:, 1],
             linewidth=.3)
    plt.plot(bezier_points[:, 0],
             bezier_points[:, 1],
             color="r",
             linewidth=.6,
             label='bezier curve')
    plt.title("Bezier curve of degree %d"%(len(control_points)-1))
    plt.legend()
    #plt.show()
    
#------------------------------------------------------------------------------

def test():
    zzz = Bezier(np.random.uniform(size=(3, 2)), 3) # numba warmup
    count = 0
    p = 30
    pos = 0
    timings = np.empty(shape=(p*p))
    lst = np.linspace(2, 1000, p, dtype=int)
    for N in lst:
        for C in lst:
            control = np.random.randint(3, 10, size=(N, 2))
            t1 = pc()
            a = Bezier(control, C)
            t1 = pc()-t1
            t2 = pc()
            b = Bezier2(control, C)
            t2 = pc()-t2
            if t2 < t1:
                count += 1
            timings[pos] = t1-t2
            pos += 1
            print(pos, N, C, end='\r', flush=True)
    print()
    print("-"*30, "\n", count, len(lst)**2)
    plt.stem(timings)
    plt.show()
    return None    
